package utils

import (
	"gitee.com/frinter_self/go_toolset/errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

func HttpResponse(ctx *gin.Context,out interface{}, err error,lang errors.IErrorLang) {
	var ses errors.StoreErrors
	if err!=nil {
		switch t:=err.(type) {
		case errors.StoreErrors:
			ses=t
		default:
			ses=errors.ErrorToStoreErrors(err,lang)
		}
		ctx.JSON(http.StatusOK, setResponse(1, ses.ErrorsToMap(), ""))
		return
	}
	ctx.JSON(http.StatusOK, setResponse(0, "success", out))
}

func setResponse(error int64, message interface{}, data interface{}) gin.H {
	rs := gin.H{
		"error":   error,
		"message": message,
		"data":    data,
	}
	return rs
}

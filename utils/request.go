package utils

import (
	"io"
	"net/http"
)

func Request(method string,urlString string,body io.Reader)(*http.Response,error){
	rq, err := http.NewRequest(method,urlString, body)
	if err!=nil{
		return nil,err
	}

	client := &http.Client{}
	resp, err := client.Do(rq)
	if err!=nil{
		return nil,err
	}
	return resp,nil
}
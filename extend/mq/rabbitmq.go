package mq

import (
	"github.com/streadway/amqp"
)

func InitRabbitMq(attr string) (*amqp.Connection, error) {
	MqConn, err := amqp.Dial(attr)
	return MqConn, err
}
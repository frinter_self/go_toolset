package jwt

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

var (
	// Define a secure key string used
	// as a salt when hashing our tokens.
	// Please make your own way more secure than this,
	// use a randomly generated md5 hash or something.
	key = []byte("mySuperSecretKeyLol")
)

// CustomClaims is our custom metadata, which will be hashed
// and sent as the second segment in our JWT
type CustomClaims struct {
	Info interface{}
	jwt.StandardClaims
}

type Authable interface {
	Decode(token string) (*CustomClaims, error)
	Encode(info interface{},expiresAt int64) (string, error)
}

type TokenService struct {
}

func NewTokenService() *TokenService {
	return &TokenService{}
}
// Decode a token string into a token object
func (srv *TokenService) Decode(tokenString string) (*CustomClaims, error) {
	// Parse the token
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})
	// Validate the token and return the custom claims
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, err
	}
}
// Encode a claim into a JWT
func (srv *TokenService) Encode(info interface{},expiresAt int64) (string, error) {
	expireToken := time.Now().Add(time.Hour * time.Duration(expiresAt)).Unix()
	// Create the Claims
	claims := CustomClaims{
		info,
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "wuding.co",
		},
	}
	// Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Sign token and return
	return token.SignedString(key)
}
package gorm

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitGormIoForMysql(mysqlAddr string) (*gorm.DB ,error){
	return gorm.Open(mysql.Open(mysqlAddr),&gorm.Config{})
}
package gorm

import (
	"github.com/jinzhu/gorm"
	"time"
)

type (
	ModelA struct {
		ID int32 `gorm:"primary_key;unsigned;AUTO_INCREMENT"`
	}
	ModelB struct {
		ModelA
		CreatedAt time.Time `gorm:"not null;type:timestamp;default:CURRENT_TIMESTAMP;comment:'创建日期'"`
	}
	ModelC struct {
		ModelB
		UpdatedAt time.Time `gorm:"not null;type:timestamp;default:CURRENT_TIMESTAMP;comment:'更新日期'"`
	}
	ModelD struct {
		ModelC
		DeletedAt *time.Time `gorm:"type:timestamp;index;comment:'删除日期'"`
	}
)

func InitJinzhuLog(mysqlAddr string) (*gorm.DB,error){
	db,err:=InitJinzhu(mysqlAddr)
	if err!=nil{
		return nil,err
	}
	db.LogMode(true)
	return db,nil
}
func InitJinzhu(mysqlAddr string) (*gorm.DB,error) {
	return gorm.Open("mysql", mysqlAddr)
}
package config

import (
	"fmt"
	"gitee.com/frinter_self/go_toolset/utils"
	"github.com/spf13/viper"
	"log"
)

func InitConfig(name string,path string)  {
	ROOT := utils.GetROOT()
	if path=="."{
		s:=[]byte(utils.GetDir())
		m:=string(s[:4])
		rs:=[]byte(ROOT)
		rm:=string(rs[:4])
		if m==rm{
			path=utils.GetDir()
		}else{
			path=ROOT+"/configs"
		}
		fmt.Println(ROOT)
	}
	fmt.Println("配置文件路径:",path)
	viper.SetDefault("root", ROOT)
	viper.AddConfigPath(path)
	viper.SetConfigName(name)
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Panic(err)
	}
}

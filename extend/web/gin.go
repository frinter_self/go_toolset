package web

import (
	"gitee.com/frinter_self/go_toolset/errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

func InItGinDefault() *gin.Engine {
	g:=gin.Default()
	g.Use(cors())
	return g
}

func InitGin() *gin.Engine {
	g:=gin.New()
	g.Use(cors())
	return g
}

type MyHandler func(gin *gin.Context) (error,interface{})

func Handler(errMap map[string]string) func(h MyHandler)gin.HandlerFunc{
	return func(h MyHandler) gin.HandlerFunc {
		return func(context *gin.Context) {
			err,rs:=h(context)
			if err!=nil{
				ces:=errors.ErrorToCustomErrors(err,errMap)
				context.JSON(http.StatusOK,gin.H{"error":1,"message":ces.ToMap(),"data":nil})
				return
			}
			context.JSON(http.StatusOK,gin.H{"error":0,"message":nil,"data":rs})
		}
	}
}

type MyMiddleWare func(gin *gin.Context) error
func Middleware(errMap map[string]string) func(h MyMiddleWare)gin.HandlerFunc{
	return func(h MyMiddleWare) gin.HandlerFunc {
		return func(context *gin.Context) {
			err:=h(context)
			if err!=nil{
				ces:=errors.ErrorToCustomErrors(err,errMap)
				context.JSON(http.StatusOK,gin.H{"error":1,"message":ces.ToMap(),"data":nil})
				context.Abort()
			}
			context.Next()
		}
	}
}

func cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")

		//放行所有OPTIONS方法
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		// 处理请求
		c.Next()
	}
}
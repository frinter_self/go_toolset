package web

import (
	"gitee.com/frinter_self/go_toolset/errors"
	"github.com/gin-gonic/gin"
	"testing"
)

func TestGin_Handler(t *testing.T)  {
	g:=InitGin()
	g.GET("",Handler(map[string]string{})(func(gin *gin.Context) (e error, i interface{}) {
		e=errors.NewCustomError("102","ceshi cuow ")
		i=nil
		return
	}))
	_=g.Run(":8989")
}
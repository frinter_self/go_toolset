package micro

import (
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/web"
)

func InitGoMicroWeb(op ...web.Option) web.Service{
	return web.NewService(op...)
}

func InitGoMicro(op ...micro.Option) micro.Service {
	return micro.NewService(op...)
}
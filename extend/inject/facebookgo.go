package inject

import (
	"github.com/facebookgo/inject"
	"log"
)

//使用依赖注入

func SetInject(objs ...interface{}){
	var i inject.Graph
	var s []*inject.Object
	for _,obj:=range objs{
		s=append(s, &inject.Object{Value:obj})
	}
	if err:=i.Provide(s...);err!=nil{
		log.Panic("inject Provide error:",err)
	}
	if err:=i.Populate();err!=nil{
		log.Panic("inject Populate error:",err)
	}
}
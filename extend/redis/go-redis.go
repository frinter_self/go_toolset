package redis

import (
	"github.com/go-redis/redis"
	"log"
)

func InitRedis(host string,password string,db int) *redis.Client {
	redisOptions := redis.Options{
		Addr:     host,
		Password: password,
		DB:       db,
	}
	rdsClient := redis.NewClient(&redisOptions)
	if _, err := rdsClient.Ping().Result(); err != nil {
		log.Panic(err)
	}
	return rdsClient
}
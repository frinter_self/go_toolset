package validate

import (
	"github.com/go-playground/validator/v10"
	"regexp"
)

const prefix = "d_"

func InitValidator() *validator.Validate {
	v := validator.New()
	_ = v.RegisterValidation(prefix+"is_phone", IsPhone)
	_ = v.RegisterValidation(prefix+"is_sex", IsSex)
	_ = v.RegisterValidation(prefix+"is_password", IsPassword)
	_ = v.RegisterValidation(prefix+"is_qq", IsQq)
	_ = v.RegisterValidation(prefix+"is_order", IsOrder)
	return v

}

func IsPhone(fl validator.FieldLevel) bool {
	s := fl.Field().String()
	o, _ := regexp.Match("^1[1-9]\\d{9}$", []byte(s))
	return o
}

func IsPassword(fl validator.FieldLevel) bool {
	s := fl.Field().String()
	//密码规则：长度6-18位  字符串类型：数字 英文 特殊符号且必须包含数字及英文
	o1, _ := regexp.Match("^[\\da-zA-Z~!@#$%^&*]{6,18}$", []byte(s))
	o2, _ := regexp.Match("[a-zA-Z]+", []byte(s))
	o3, _ := regexp.Match("\\d+", []byte(s))
	return o1 && o2 && o3
}

func IsQq(fl validator.FieldLevel) bool {
	s := fl.Field().String()
	o, _ := regexp.Match("^\\d{4,20}$", []byte(s))
	return o
}

func IsOrder(fl validator.FieldLevel) bool {

	return false
}

func IsSex(fl validator.FieldLevel) bool {
	s := fl.Field().Int()
	return s == 1 || s == 2
}

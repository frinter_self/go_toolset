package errors

import (
	"bytes"
	"fmt"
	"strings"
)

type (
	IStoreError interface {
		ErrorCode() string
		ErrorMessage() string
	}

	IErrorLang interface {
		ErrorName() string
		ErrorMap() map[string]string
		ErrorString(code string) string
	}

	StoreError struct {
		code    string
		message string
	}

	StoreErrors []IStoreError

	BaseErrorLang struct {
		errName string
		errMap  map[string]string
	}
)

//编译时检查是否实现接口
var _ IStoreError = new(StoreError)
var _ error = new(StoreError)

var BaseErrorMap=map[string]string{
	"unknown":"未知错误",
}

func NewBaseErrorLang() *BaseErrorLang {
	errMap := &BaseErrorLang{
		errName:"base.error.",
		errMap:BaseErrorMap,
	}
	return errMap
}

func (m *BaseErrorLang) ErrorName() string {
	return m.errName
}

func (m *BaseErrorLang) ErrorMap() map[string]string {
	return m.errMap
}

func (m *BaseErrorLang) ErrorString(code string)string{
	return m.errMap[code]
}

func NewStoreError(code string,lang interface{}) *StoreError {
	se:=&StoreError{}
	baseErr:=NewBaseErrorLang()
	if code==""{
		code="unknown"
		se.code=baseErr.errName+code
		se.message=baseErr.ErrorString(code)
		return se
	}
	if lang==nil{
		se.code=baseErr.errName+code
		se.message=baseErr.ErrorString(code)
		return se
	}

	switch t:=lang.(type) {
	case IErrorLang:
		se.code=t.ErrorName()+code
		se.message=t.ErrorString(code)
	case string:
		se.code=code
		se.message=t
	default:
		se.code=code
		se.message=fmt.Sprintf("%v",lang)
	}

	return se
}


func (e *StoreError) Error() string {
	return fmt.Sprintf("%s:%s", e.code, e.message)
}

func (e *StoreError) ErrorCode() string {
	return e.code
}

func (e *StoreError) ErrorMessage() string {
	return e.message
}

func NewStoreErrors(errs ...IStoreError) StoreErrors {
	return errs
}

func (se StoreErrors) Error() string {
	buff := bytes.NewBufferString("")

	for _, fe := range se {
		buff.WriteString(fe.ErrorCode() + fe.ErrorMessage())
		buff.WriteString("\n")
	}

	return strings.TrimSpace(buff.String())
}

func (se StoreErrors) ErrorsToMap() map[string]string {

	var seMap = make(map[string]string)

	for _, e := range se {
		seMap[e.ErrorCode()] = e.ErrorMessage()
	}
	return seMap
}

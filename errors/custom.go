package errors

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/go-sql-driver/mysql"
	lacia "github.com/jialanli/lacia/utils"
	"strings"
)

type CustomError struct {
	Code    string
	Message string
}

func NewCustomError(code string, message string) *CustomError {
	return &CustomError{Code: code, Message: message}
}

func (m *CustomError) Error() string {
	return fmt.Sprintf("[%s]%s", m.Code, m.Message)
}

type CustomErrors []*CustomError

func (l CustomErrors) Error() string {
	buff := bytes.NewBufferString("")
	for _, fe := range l {
		buff.WriteString(fe.Error())
		buff.WriteString("\n")
	}
	return strings.TrimSpace(buff.String())
}

func (l CustomErrors) ToMap() map[string]string {
	var seMap = make(map[string]string)
	for _, e := range l {
		seMap[e.Code] = e.Message
	}
	return seMap
}
func (l CustomErrors) ToList() []string {
	var seList = make([]string, 0)
	for _, e := range l {
		seList = append(seList, e.Message)
	}
	return seList
}

func ErrorToCustomErrors(err error, lang map[string]string) CustomErrors {
	var code string
	var errs CustomErrors
	//判断错误类型进行处理
	switch t := err.(type) {
	case validator.ValidationErrors: //validator错误处理
		for _, f := range t {
			key := f.Namespace()
			keyArr := strings.Split(key, ".")
			if len(keyArr) > 1 {
				code = strings.Join(keyArr[1:], "")
			} else {
				code = key
			}
			//code += "." + f.Tag()
			code=lacia.RemoveX(code,"_")
			code=strings.ToUpper(code)
			errs = append(errs, NewCustomError(code, lang[code]))
		}
		return errs
	case *json.UnmarshalTypeError: //json转换Struct错误处理
		code = strings.ToUpper(t.Field)
		code=lacia.RemoveX(code,"_")
		errs = []*CustomError{NewCustomError(code, lang[code])}
		return errs
	case *mysql.MySQLError:
		code = fmt.Sprintf("SQL%d",int(t.Number))
		errs = []*CustomError{NewCustomError(code, lang[code])}
		return errs
	case *CustomError: //同类型错误直接转换
		errs = []*CustomError{t}
		return errs
	default: //其它错误处理
		code = "UNKNOWN"
		errs = []*CustomError{NewCustomError(code, lang[code])}
		return errs
	}
}

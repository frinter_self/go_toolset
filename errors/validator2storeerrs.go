package errors

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/go-sql-driver/mysql"
	"strconv"
	"strings"
)

func ErrorToStoreErrors(err error, lang IErrorLang) StoreErrors {
	var code string
	//判断错误类型进行处理
	switch t := err.(type) {
	case validator.ValidationErrors: //validator错误处理
		storeErrors:=NewStoreErrors()
		for _,f:=range t{
			key := f.Namespace()
			keyArr := strings.Split(key, ".")
			if len(keyArr) > 1 {
				code = strings.Join(keyArr[1:], ".")
			} else {
				code = key
			}
			code += "." + f.Tag()
			storeErrors=append(storeErrors,NewStoreError(code,lang))
		}
		return storeErrors
	case *json.UnmarshalTypeError: //json转换Struct错误处理
		code = t.Field
		return NewStoreErrors(NewStoreError(code,lang))
	case *mysql.MySQLError:
		code = strconv.Itoa(int(t.Number))
		return NewStoreErrors(NewStoreError(code,lang))
	case *StoreError: //同类型错误直接转换
		return NewStoreErrors(t)
	default: //其它错误处理
		return NewStoreErrors(NewStoreError(code,err.Error()))
	}
}


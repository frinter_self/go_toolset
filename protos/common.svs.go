package protos

import (
	"database/sql/driver"
	"fmt"
	"time"
)

const (
	timeLayout = "2006-01-02 15:04:05"
)

func (m *Timestamp) Scan(value interface{}) error {
	switch t := value.(type) {
	case time.Time:
		m.Unix = t.Unix()
		m.Datetime = t.Format(timeLayout)
	default:
		return fmt.Errorf("timestamp data error")
	}
	return nil
}

//timestemp类型处理
func (m *Timestamp) Value() (driver.Value, error) {
	var t driver.Value
	var err error
	if m != nil {
		t = time.Unix(m.Unix, 0)
	}
	return t, err
}

//sex类型处理
func (m *Sex) Scan(value interface{}) error {
	switch t := value.(type) {
	case int64:
		return m.IntToSexEnum(int32(t))
	case int32:
		return m.IntToSexEnum(t)
	case int:
		return m.IntToSexEnum(int32(t))
	default:
		return fmt.Errorf("sex data error")
	}
}

func (m *Sex) Value() (driver.Value, error) {
	var t driver.Value
	if m != nil {
		t = m.Num
	}
	return t, nil
}

func (m *Sex) IntToSexEnum(i int32) error {
	s:=SexEnum(i)
	m.Num=s
	m.Str=SexEnum_name[i]
	return nil
}
